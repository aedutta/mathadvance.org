---
title: Contribute to Math Advance
desc: Information and guidelines for prospective contributors to Math Advance.
---

# Contribute

Thank you for your interest in contributing to Math Advance! This page contains some guidelines to ensure the process goes as smoothly as possible.

## Presidential Service Award

We are certified to sign off on volunteer hours and submit them for the [PVSA](https://presidentialserviceawards.gov/). We submit at October 1 every year. If you've worked with Math Advance, email me for more specific instructions.

## Polish

Your work must be polished in every sense. Say you are writing a handout:

- Your prose must flow smoothly. When you spot awkward wording, it must be fixed even if it does not directly impact the reader's understanding.
- If applicable, semantic elements must not be overused. Not everything should be wrapped inside a theorem environment. This is a good heuristic for your writing because it helps prevent your writing from degenerating into "list all the theorems and jump straight to examples". It helps ensure your text flows smoothly and that you are actually providing insights, not just facts.
- The LaTeX must be written well. Whitespace must be used to ensure that complex environments like 'align' remain readable.

Even though such strict standards may ultimately bring diminishing returns, polish is an *attitude* that we hold. We want our work to be as good as possible and will not be lazy just because "it's good enough" or "we're already doing better than everyone else".

The sections that follow describe in more detail the expectations for tooling that you may use along with some tips on how to best use it.

## LaTeX and Asymptote

You *must* have LaTeX installed locally, otherwise we will reject your contributions. This is not out of some misplaced sense of elitism. It is due to the limited manpower we have and because our biggest bottleneck is not a lack of content, it is a lack of polished content. Our recent work with our teaching materials has largely been polishing old handouts, not writing new ones.

We require LaTeX to be installed locally because that way, contributors can apply file formatting scripts on their code. I personally recommend my own script [latex-lint](https://gitlab.com/chennisden/dotfiles/bin/latex-lint).

The biggest mistake LaTeX newbies make is using double dollar signs to delineate display math environments, like `$$1+1=2$$`. This is objectively incorrect and must not, under any circumstances, be used. Instead, write `\[1+1=2\]`. It is hard to programmatically fix (which is why I don't include it with latex-lint), partially because there is no guarantee that a TeX file written by someone who would make such a novice mistake will be correct anyways.

My personal website contains several articles on LaTeX and Asymptote. It is a good idea to read all of them, particularly the first three. Some way or another, learn how latexmk and Asymptote work.

- [LaTeX for Beginners](https://dennisc.net/writing/tech/latex)
- [Why latexmk?](https://dennisc.net/writing/tech/latexmk)
- [Asymptote is the best Asymptote compiler](https://dennisc.net/writing/tech/asy)
- [A High-Level Overview of TeX](https://dennisc.net/writing/tech/tex-overview)

For those who like to learn by example, [the source for MAST's Perspectives](https://gitlab.com/mathadvance/mast/units/cqv-perspectives) is available.

## Git

Your git commits should be [a perfect, atomic unit of change](https://portal.mozz.us/gemini/drewdevault.com/2021/01/02/2021-01-02-Every-commit-should-be-perfect.gmi) and the commit messages must describe said change. You should also use [git add -p](https://dennisc.net/writing/tech/git-add-p) and its cousins, `git commit -p`, `git add -i`, etc in order to only stage parts of a file.

It's impossible to get all your commits perfect the first time around, and in many cases agonizing over commits before you've finished developing your feature or fixing a bug can be a waste of time. Fortunately, `git rebase` can help you rewrite your history after the fact, allowing you the power of hindsight when deciding what your commits should look like. Here is what an effective Git workflow looks like:

- Make whatever changes you need on your own private tree, making some commits to roughly delineate each change. ("Private" doesn't mean "no one can see it", it just means "no one is depending on it".) At this point your commit history might not be perfect, and it might be a steaming ball of crap. Both are okay.
- Use `git rebase -i` to clean up your commit history.
- Send your changes to upstream, either through a merge request or an email. (Don't send a ton of changes upstream at once; split changes up into relevant chunks and communicate with me so that rebasing isn't painful for you and code review isn't painful for me!)

[Linus Torvalds](https://lwn.net/Articles/328438/) has a famous email about `git rebase` and `git merge`. It's not directly relevant to the work we do, since our projects are much smaller. But one of the points he makes is that you should only rebase so long as no one depends on your Git history, and that holds true here as well.

An example of good commit history is the [mapm library](https://git.sr.ht/~dennisc/mapm-lib). Our software projects aren't as big as Postgres or Linux, so we don't need commit messages as descriptive, but they are the gold standard.

Note that commit discipline eases up a little bit when the repository is not a software project, as can be seen in the commit history for [CQV-Perspectives](https://gitlab.com/mathadvance/mast/units/cqv-perspectives/-/commits/master). However, the general principles still apply. The commits still describe the changes, and barring mistakes, each commit is an atomic unit of change.

## Web Development

Math Advance websites must be developed conservatively, using simple HTML and CSS. Javascript is absolutely forbidden, because everything it provides can instead be implemented more effectively through HTML or CSS (except for live timers which have no suitable replacement in HTML or CSS). It is bloated and serves to make the website worse for the client, with minimal benefits server-side. Using plain HTML and CSS is better for longevity too, because unlike the next Javascript framework, it will not fall out of fashion.

You do *not* need a static site generator either. The themes are overly complex and the code that runs them is a [behemoth](https://github.com/jekyll/jekyll). Static site generators are flexible and serve many use-cases, the vast majority of which you will not need. Your code does not need to generalize for other websites because you aren't writing them.

This website is the gold standard. It uses `nav`, `main`, and `footer` tags to make the website more accessible to screen readers. For internationalization, it has the `lang` property set and `charset` is specified through the response headers. The build script is 5 lines of shell and offloads work to mature tools like pandoc when necessary. The CSS is not even 100 lines long and we still have support for dark mode. It gets full points in every category of Google's Lighthouse tests, and it is fully WCAG compliant.

When a backend and API are necessary, they should ideally be written in a static typing language like Rust. Prefer to use languages and tools that make things explicit and consistent.

We have not always abided by these principles in the past and are suffering for the consequences for it now. However, having a mess in some of our codebases does not give us free license to make the problem worse. Therefore, these conservative web development principles will be strictly enforced henceforth.
