.PHONY: all html

all: build build/favicon.ico build/main.css html

build:
	mkdir build

clean: build
	rm -r build/*.html

html:
	./generate-html.sh

build/favicon.svg: build
	ln -s ../favicon.svg build/favicon.svg	

build/main.css: build
	ln -s ../main.css build/main.css
