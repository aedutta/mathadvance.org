find content -print | grep .md | while IFS= read -r MD_PATH; do
	pandoc --template=html.template $MD_PATH > $(echo $MD_PATH | sed 's/content/build/' - | sed 's/.md/.html/' -)
done
